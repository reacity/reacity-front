var map;
			var circles;
			var osm;
			/** 
				@function initmap
				@desc Map initialization on body load
				@author Realicity
			*/
			function initmap() {
				
				// set up the map
				map = new L.Map('map');
				
				// create the tile layer with correct attribution
				var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
				var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
				osm = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: 18, attribution: osmAttrib});
				
				
				map.setView(new L.LatLng(CITYLAT, CITYLONG),12);
				map.addLayer(osm);
				
				getAllAreas();
				
				map.on('click', addArea);	
			}
			/** 
				@function getAllAreas
				@desc Get all areas available on server
				@author Realicity
			*/
			function getAllAreas()
			{
				$.ajax(
					{
						url:SERVERURL+":"+SERVERPORT+"/reacity-core-1.0-SNAPSHOT/api/info/get",
						async:false,
					}
				)
				.done(function(data) {
					circles = data;
				})
				.fail(function() {
					alert( "error" );
				});
				drawCircles();
			}
			/** 
				@function drawCircles
				@desc Draw areas on the map
				@author Realicity
			*/
			function drawCircles()
			{
				map.eachLayer(function (layer) {
					if(layer != osm)
						map.removeLayer(layer);
				});
				circles.forEach(function(e)
					{
						var circle = L.circle([e.latitude, e.longitude], {
							color: 'blue',
							fillColor: '#30f',
							fillOpacity: 0.2,
							radius: e.radius
						});
						circle.on('click', function(ev)
						{
							// dynamical color change
							map.eachLayer(function (layer) {
								if(layer != osm)
								{
									map.removeLayer(layer);
									layer.options.color = "blue";
									layer.options.fillColor="#30f";
									layer.addTo(map);
								}
								if(layer == circle)
								{
									map.removeLayer(layer);
								}
							});
							
							circle.options.color="red";
							circle.options.fillColor="#f03";
							circle.addTo(map);
							// form to fill
							document.getElementById("nom").value = e.name;
							document.getElementById("latitude").value = e.latitude;
							document.getElementById("longitude").value = e.longitude;
							document.getElementById("radius").value = e.radius;
							document.getElementById("description").value = e.information;
						});
						circle.addTo(map);
					}
				);
			}
			/** 
				@function changeRadius
				@desc Dynamically change radius on the map
				@author Realicity
			*/
			function changeRadius(textbox)
			{
				circles.forEach(function(e)
				{
					if(e.latitude == document.getElementById('latitude').value && e.longitude == document.getElementById('longitude').value)
					{
						e.radius = document.getElementById("radius").value;
						drawCircles();
					}
				});
			}
			/** 
				@function addArea
				@desc Add an area on the map
				@author Realicity
			*/
			function addArea(e)
			{
				if(document.getElementById('add').checked)
				{
					var dataToSend = '{"name": "Nouveau Quartier","latitude": '+ e.latlng.lat+',"longitude":'+ e.latlng.lng+',"radius": 500,"information": "Bienvenue!"}';
					$.ajax(
						{
							url:SERVERURL+":"+SERVERPORT+"/reacity-core-1.0-SNAPSHOT/api/info/add",
							type:"POST",
							dataType:DATATYPE,
							contentType: CONTENTTYPE,
							data: dataToSend
						}
					)
					.done(function(data) {
						getAllAreas();
						document.getElementById('alert-container').innerHTML = '<div class="alert alert-success fade in">Nouveau Quartier créé avec succès!</div>';
					})
					.fail(function() {
						document.getElementById('alert-container').innerHTML = '<div class="alert alert-danger fade in">Erreur lors de la création</div>';
					});
				}
				document.getElementById('add').checked=false;
			}
			/** 
				@function modifyArea
				@desc Modify an area on the map
				@author Realicity
			*/
			function modifyArea(e)
			{
				var dataToSend = '{"name": "'+ document.getElementById('nom').value +'","latitude": '+ document.getElementById('latitude').value +',"longitude":'+ document.getElementById('longitude').value +',"radius": '+ document.getElementById('radius').value +',"information": "'+ document.getElementById('description').value +'"}';
				$.ajax(
					{
						url:SERVERURL+":"+SERVERPORT+"/reacity-core-1.0-SNAPSHOT/api/info/update",
						type:"POST",
						dataType:DATATYPE,
						contentType: CONTENTTYPE,
						data: dataToSend
					}
				)
				.done(function(data) {
					getAllAreas();
					document.getElementById('alert-container').innerHTML = '<div class="alert alert-success fade in">Modification effectuée avec succès</div>';
				})
				.fail(function() {
					document.getElementById('alert-container').innerHTML = '<div class="alert alert-danger fade in">Erreur lors de la modification</div>';
				});
				
			}
			/** 
				@function deleteArea
				@desc Delete an area on the map
				@author Realicity
			*/
			function deleteArea(e)
			{
				var dataToSend = '{"latitude": '+ document.getElementById('latitude').value +',"longitude":'+ document.getElementById('longitude').value +'}';
				$.ajax(
					{
						url:SERVERURL+":"+SERVERPORT+"/reacity-core-1.0-SNAPSHOT/api/info/delete",
						type:"POST",
						dataType:DATATYPE,
						contentType: CONTENTTYPE,
						data: dataToSend
					}
				)
				.done(function(data) {
					getAllAreas();
					document.getElementById('alert-container').innerHTML = '<div class="alert alert-success fade in">Suppression effectuée avec succès</div>';
				})
				.fail(function() {
					document.getElementById('alert-container').innerHTML = '<div class="alert alert-danger fade in">Erreur lors de la suppression</div>';
				});
			}
			